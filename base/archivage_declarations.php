<?php
/**
 * Ce fichier contient les modifications de la base de données requises par le plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des nouveaux champs et des nouvelles tables de la base de données propres au plugin.
 *
 * Le plugin ne déclare aucune nouvelle table mais ajoute des champs aux tables d'objet :
 *
 * - `est_archive`, 0 pour non archivé, 1 pour archivé,
 * - `date_archive`, la date d'archive ou la date de fin d'archive suivant l'état précédent,
 * - `objet_racine_archive`, type d'objet du parent ayant propagé son archivage ou désarchivage,
 * - `id_racine_archive`, l'id du parent ayant propagé son archivage ou désarchivage,
 * - `motif_archive`, identifiant unique pour un motif (les motifs sont propres aux plugins utilisateur).
 *
 * @param array $tables Liste des tables et de leur configuration
 *
 * @return array Liste des tables mise à jour
 */
function archivage_declarer_tables_objets_sql(array $tables) : array {
	// On ajoute la déclaration des champs nécessaires à l'archivage pour toutes les tables d'objet déjà déclarées.
	include_spip('inc/archivage');
	foreach (_ARCHIVAGE_CHAMPS_SQL as $_champ => $_declaration) {
		$tables[]['field'][$_champ] = $_declaration;
	}

	return $tables;
}
