<?php
/**
 * Gestion du formulaire de modification du motif d'archivage ou de désarchivage.
 *
 **/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement du formulaire de modification du motif d'archivage ou de désarchivage.
 *
 * @param string $objet    Type de l'objet concerné
 * @param int    $id_objet Identifiant de l'objet concerné
 * @param string $redirect URL de redirection suite à l'édition
 *
 * @return array Contexte de chargement à fournir au formulaire
 */
function formulaires_editer_motif_archivage_charger(string $objet, int $id_objet, string $redirect) : array {
	// L'éditabilité : autorisation de modifier l'archivage
	include_spip('inc/autoriser');
	$editable = autoriser(
		'modifierarchivage',
		$objet,
		$id_objet,
		null,
		['action' => 'expliquer']
	);

	// Construction de la liste des motifs
	// -- récupération de l'état d'archivage de l'objet concerné
	include_spip('inc/archivage');
	$archivage = archivage_objet_lire(
		$objet,
		$id_objet
	);

	// -- constitution de la liste des motifs en fonction du type d'objet et de l'état d'archivage
	$motifs = archivage_motif_lister($objet, $archivage['etat']);

	// Constitution du tableau des variables du formulaire.
	return [
		'editable'      => $editable,
		'_motif_label'  => _T('archivage:edition_motif_label'),
		'_motifs'       => $motifs,
		'_motif_defaut' => archivage_motif_defaut($objet, $archivage['etat']),
		'motif'         => $archivage['motif_archive'] ?? ''
	];
}

/**
 * Traiter la modification de l'édition du motif.
 *
 * @param string $objet    Type de l'objet concerné
 * @param int    $id_objet Identifiant de l'objet concerné
 * @param string $redirect URL de redirection suite à l'édition
 *
 * @return array Tableau de sortie du formulaire (messages, redirection, etc.)
 */
function formulaires_editer_motif_archivage_traiter(string $objet, int $id_objet, string $redirect) : array {
	// Initialisation du retour
	$retour = [];

	if (
		include_spip('inc/autoriser')
		and autoriser('modifierarchivage', $objet, $id_objet, null, ['action' => 'expliquer'])
	) {
		// Acquérir le motif choisi et mise à jour en utilisant l'action de modification idoine.
		$motif = _request('motif');
		include_spip('inc/archivage');
		if (archivage_objet_modifier('expliquer', $objet, $id_objet, $motif)) {
			$autoclose = '<script type="text/javascript">if (window.jQuery) jQuery.modalboxclose();</script>';
			$retour['message_ok'] = _T('info_modification_enregistree') . $autoclose;
			$retour['redirect'] = $redirect ?: '';
		} else {
			$retour['message_erreur'] = _T('archivage:erreur_modifier_archivage_motif');
		}
	} else {
		$retour['message_erreur'] = _T('archivage:erreur_modifier_archivage_non_autorisee');
	}

	return $retour;
}
