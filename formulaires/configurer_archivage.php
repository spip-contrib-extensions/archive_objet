<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données : le formulaire permet de paramétrer le plugin Archivage de Contenus.
 *
 * @return array Le tableau des données à charger par le formulaire de configuration.
 */
function formulaires_configurer_archivage_charger() : array {
	// Chargement des données de configuration déjà en meta
	include_spip('inc/cvt_configurer');
	$valeurs = cvtconf_formulaires_configurer_recense('configurer_archivage');

	// Ajouter la liste des tables archivables
	include_spip('inc/filtres');
	include_spip('inc/archivage');
	$tables_archivables = archivage_table_lister();
	foreach ($tables_archivables as $_table => $_config_enfants) {
		$message_enfants = '';
		if ($_config_enfants) {
			foreach (array_keys($_config_enfants) as $_type_enfant) {
				$message_enfants .= ($message_enfants ? ', ' : '') . _T(objet_info($_type_enfant, 'texte_objets'));
			}
			if ($message_enfants) {
				$message_enfants = ' (' . _T('archivage:configuration_tables_autorisees_enfants', ['enfants' => $message_enfants]) . ')';
			}
		}
		$valeurs['_tables_archivables'][$_table] = _T(objet_info($_table, 'texte_objets')) . $message_enfants;
	}

	// Indicateur d'éditabilité
	$valeurs['editable'] = true;

	return $valeurs;
}

/**
 * Vérification des saisies : si on choisit un type d'objet avec enfants, les enfants doivent aussi être sélectionnés.
 *
 * @return array Messages d'erreur éventuels
 */
function formulaires_configurer_archivage_verifier() : array {
	// Initialisation des erreurs de saisie
	$erreurs = [];
	$tables_erreurs = '';

	// Récupérer la saisie des tables pour la vérifier
	$tables_autorisees = _request('tables_autorisees');

	// Si une table choisie possède des enfants on vérifie que les enfants sont également autorisés
	// -- si ce n'est pas le cas, on monte une erreur
	include_spip('base/objets');
	include_spip('inc/archivage');
	$tables_archivables = archivage_table_lister();
	foreach ($tables_autorisees as $_table) {
		if (
			$_table
			and $tables_archivables[$_table]
		) {
			foreach (array_keys($tables_archivables[$_table]) as $_objet) {
				if (!in_array(table_objet_sql($_objet), $tables_autorisees)) {
					$tables_erreurs .= ($tables_erreurs ? ', ' : '') . _T(objet_info($_objet, 'texte_objets'));
				}
			}
		}
	}

	// Traiter l'erreur de saisie des tables autorisées
	if ($tables_erreurs) {
		$erreurs['tables_autorisees'] = _T('archivage:configuration_tables_autorisees_erreur', ['objets' => $tables_erreurs]);
	}

	return $erreurs;
}

/**
 * Exécuter les traitements : ne pas stocket le hodden des tables autorisées.
 *
 * @return array Tableau des messages de bon traitement ou d'erreur.
 */
function formulaires_configurer_archivage_traiter() : array {
	$retour = [];

	// Récupérer la saisie des tables à autoriser
	$tables_a_autoriser = _request('tables_autorisees');
	// Suppression de la saisie hidden qui correspond à une table vide
	foreach ($tables_a_autoriser as $_cle => $_table) {
		if (!$_table) {
			unset($tables_a_autoriser[$_cle]);
			break;
		}
	}
	set_request('tables_autorisees', $tables_a_autoriser);

	// Détermination des changements de paramètres qui nécessitent une réinitialisation partielle ou totale du
	// contexte d'archivage de certains objets
	// -- Tables archivables
	include_spip('inc/archivage');
	$tables_archivables = archivage_table_lister();

	// -- lecture de la configuration actuellement enregistrée (avant modification)
	include_spip('inc/config');
	$parametrage_actuel = lire_config('archivage', []);
	$post_traitements = [];
	// -- les tables autorisées
	foreach ($parametrage_actuel['tables_autorisees'] as $_table) {
		if (
			!in_array($_table, $tables_a_autoriser)
			and array_key_exists($_table, $tables_archivables)
		) {
			// La table à été supprimée de la liste des tables autorisées : on le consigne
			$post_traitements['desactiver_tables']['tables'][] = $_table;
		}
	}
	// -- Propagation aux enfants
	include_spip('inc/archivage');
	include_spip('base/objets');
	if (!empty($post_traitements['desactiver_tables']['tables'])) {
		foreach ($post_traitements['desactiver_tables']['tables'] as $_table) {
			if ($tables_archivables[$_table]) {
				// La table a des enfants : pour chaque enfant qui n'est pas dans desactiver_tables on demande la
				// réinitialisation de type enfant (en effet les tables de desactiver_tables seront déjà entièrement mise à zéro)
				foreach (array_keys($tables_archivables[$_table]) as $_objet) {
					$table_sql_enfant = table_objet_sql($_objet);
					if (
						($table_sql_enfant !== $_table)
						and !in_array($table_sql_enfant, $post_traitements['desactiver_tables']['tables'])
					) {
						$post_traitements['desactiver_enfants']['tables'][] = $table_sql_enfant;
						$post_traitements['desactiver_enfants']['parent'] = objet_type($_table);
					}
				}
			}
		}
	}

	// -- la consignation du désarchivage
	foreach (['consigner_desarchivage', 'utiliser_motif'] as $_variable) {
		if (
			($parametrage_actuel[$_variable] == 'on')
			and !_request($_variable)
		) {
			// On utilise plus le paramètre, on le consigne pour toutes les tables autorisées
			$post_traitements[$_variable]['tables'] = [];
		}
	}

	// On enregistre d'abord les nouvelles valeurs saisies.
	include_spip('inc/cvt_configurer');
	cvtconf_formulaires_configurer_enregistre('configurer_archivage', []);

	// On vérifie que les tables autorisées possèdent bien les champs du contexte d'archivage
	// -- cas des tables créées après l'installation du plugin Archivage de Contenus
	$trouver_table = charger_fonction('trouver_table', 'base');
	foreach ($tables_a_autoriser as $_table_sql) {
		// On teste l'existence du contexte d'archivage en testant juste le champ est_archive
		$desc = $trouver_table($_table_sql);
		if (!isset($desc['field']['est_archive'])) {
			archivage_table_ajouter_contexte($_table_sql);
		}
	}

	// On effectue les post-traitements permettant de réinitialiser le contexte d'archivage (partiel ou total) pour
	// un certain nombre d'objets
	$post_message = '';
	foreach ($post_traitements as $_action => $_arguments) {
		$nbs[$_action] = archivage_table_reinitialiser(
			$_action,
			$_arguments['tables'] ?? [],
			$_arguments['parent'] ?? '',
		);
		// Formatage du message
		// -- calcul du complément avec le nombre d'objets par table
		$nb = '';
		foreach ($nbs[$_action] as $_table => $_nb) {
			$nb = ($nb ? ', ' : '') . _T(objet_info(objet_type($_table), 'texte_objets')) . ' [' . $_nb . ']';
		}
		if ($nb) {
			$post_message .= '<br>- ' . _T('archivage:configuration_reinit_' . $_action, ['nb' => $nb]);
		}
	}

	$retour['message_ok'] = _T('config_info_enregistree') . $post_message;
	$retour['editable'] = true;

	return $retour;
}
