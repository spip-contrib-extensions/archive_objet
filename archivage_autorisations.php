<?php
/**
 * Ce fichier contient les autorisations nécessaire à la gestion de l'archivage des objets.
 *
 * @package SPIP\ARCHIVAGE\AUTORISATIONS
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Fonction vide pour le pipeline homonyme.
 *
 * @return void
 */
function archivage_autoriser() {
}

/**
 * Autorisation minimale d'accès à toutes les pages du plugin Archivage de Contenus ou à toutes les manipulations.
 * Par défaut, seuls les administrateurs sont autorisés à utiliser ces pages.
 * Cette autorisation est à la base de la plupart des autres autorisations du plugin.
 *
 * @param string         $faire   L'action : `archivage`
 * @param string         $type    Le type d'objet ou nom de table : chaine vide
 * @param int            $id      Id de l'objet sur lequel on veut agir : 0, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_archivage_dist($faire, $type, $id, $qui, $options) {
	return ($qui['statut'] === '0minirezo');
}

/**
 * Autorisation d'accès à la page de configuration du plugin Archivage de Contenus (page=configurer_archivage).
 * Il faut être administrateur complet (autorisation par défaut de la configuration).
 *
 * A noter que la configuration du plugin est sensible : il faut donc veiller à ce que l'autorisation soit cohérente
 * avec cette sensibilité.
 *
 * @param string         $faire   L'action : `configurer`
 * @param string         $type    Le type d'objet ou nom de table : `archivage` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Id de l'objet sur lequel on veut agir : 0, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_archivage_configurer_dist($faire, $type, $id, $qui, $options) {
	return autoriser('configurer');
}

/**
 * Autorisation d'archiver, de désarchiver, de modifier le motif d'archivage ou d'effacer les données d'archivage d'un objet donné.
 * - l'archivage doit être autorisé sur le type d'objet
 * - l'utilisateur doit être un administrateur
 * - et l'objet ne doit pas être dans l'état consécutif à l'action demandée.
 *
 * @param string          $faire   Action demandée : `modifierarchivage`
 * @param string          $type    Type d'objet archivable
 * @param null|int|string $id      Identifiant de l'objet
 * @param array           $qui     Description de l'auteur demandant l'autorisation
 * @param array           $options Options de cette autorisation : l'index 'action' qui vaut `archiver` ou `desarchiver`
 *                                 ou `expliquer` ou `effacer`.
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_modifierarchivage_dist($faire, $type, $id, $qui, $options) {
	$autoriser = false;

	if (
		autoriser('archivage')
		and include_spip('inc/archivage')
		and archivage_objet_est_autorise($type)
		and ($id_objet = (int) $id)
		and isset($options['action'])
	) {
		// Acquisition du contexte d'archivage de l'objet
		$contexte_archivage = archivage_objet_lire(
			$type,
			$id_objet
		);

		// Pour chaque action...
		include_spip('inc/config');
		$action = $options['action'];
		switch ($action) {
			case 'archiver':
				if (!$contexte_archivage['est_archive']) {
					$autoriser = true;
				}
			break;
			case 'desarchiver':
				if ($contexte_archivage['est_archive']) {
					if (
						!$contexte_archivage['est_enfant']
						or (lire_config('archivage/modifier_enfant', '') === 'on')
					) {
						$autoriser = true;
					}
				}
			break;
			case 'expliquer':
				if (
					(lire_config('archivage/utiliser_motif', '') === 'on')
					and !$contexte_archivage['est_enfant']
				) {
					$motif = $contexte_archivage['motif_archive'];
					$motifs_alternatifs = archivage_motif_lister($type, $contexte_archivage['etat']);
					if ($motif) {
						$motifs_alternatifs = array_diff(array_keys($motifs_alternatifs), [$motif]);
					}
					if ($motifs_alternatifs) {
						$autoriser = true;
					}
				}
			break;
			case 'effacer':
				if (
					!$contexte_archivage['est_archive']
					and (lire_config('archivage/consigner_desarchivage', '') === 'on')
					and $contexte_archivage['date_archive']
					and ($contexte_archivage['date_archive'] !== '0000-00-00 00:00:00')
				) {
					$autoriser = true;
				}
			break;
		}
	}

	return $autoriser;
}

/**
 * Autorisation d'effacer toutes les données d'archivage de tous les objets archivables.
 * Il faut avoir l'autorisation de configurer le plugin.
 *
 * @param string          $faire   Action demandée : `vider`
 * @param string          $type    Le type d'objet ou nom de table : `archivage` (ce n'est pas un objet au sens SPIP)
 * @param null|int|string $id      Identifiant de l'objet : 0, inutilisé
 * @param array           $qui     Description de l'auteur demandant l'autorisation
 * @param array           $options Options de cette autorisation : `null`, inutilisé
 *
 * @return bool `true` si l'autorisation est donnée, `false` sinon
**/
function autoriser_archivage_vider_dist($faire, $type, $id, $qui, $options) {
	return autoriser('configurer', '_archivage');
}
