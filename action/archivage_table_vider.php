<?php
/**
 * Ce fichier contient l'action `archivage_table_vider` utilisée par un utilisateur autorisé pour
 * supprimer toute trace d'archivage dans la base.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet de supprimer toute trace d'archivage dans les tables archivables.
 *
 * Cette action est réservée aux utilisateurs possédant l'autorisation de vidage de l'archivage.
 * Elle ne nécessite aucun arguments.
 *
 * @return void
 */
function action_archivage_table_vider_dist() : void {
	// Verification des autorisations
	if (!autoriser('vider', '_archivage')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Modification de l'état d'archivage de l'objet
	include_spip('inc/archivage');
	archivage_table_reinitialiser();
}
