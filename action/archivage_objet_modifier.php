<?php
/**
 * Ce fichier contient l'action `archivage_objet_modifier` utilisée par un adminitrateur pour
 * archiver ou retirer des archives un contenu réputé archivable.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet de modifier l'état d'archivage d'un contenu.
 *
 * Cette action est réservée aux utilisateurs possédant l'autorisation de modifier l'archivage.
 * Elle nécessite plusieurs arguments, l'action, le type et l'id de l'objet et le motif (éventuellement vide).
 *
 * @param null|string $arguments Chaine des arguments de l'action séparés par un ':' ou null suivant le type d'appel
 *                               de la fonction
 *
 * @return void
 */
function action_archivage_objet_modifier_dist(?string $arguments = null) : void {
	// Récupération des arguments de façon sécurisée.
	if (null === $arguments) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arguments = $securiser_action();
	}

	// Extraction des arguments objet et id objet
	[$action, $objet, $id, $id_motif, $objet_racine, $id_racine] = explode(':', $arguments);

	if ($id_objet = (int) $id) {
		// Verification des autorisations
		if (!autoriser('modifierarchivage', $objet, $id_objet, null, ['action' => $action])) {
			include_spip('inc/minipres');
			echo minipres();
			exit();
		}

		// Modification de l'état d'archivage de l'objet
		include_spip('inc/archivage');
		$racine = [];
		if (
			$objet_racine
			and ($id = (int) $id_racine)
		) {
			$racine['id'] = $id;
			$racine['objet'] = $objet_racine;
		}
		archivage_objet_modifier($action, $objet, $id_objet, $id_motif, $racine);
	}
}
