<?php
/**
 * Ce fichier contient les fonctions d'API du plugin Archivage de Contenus.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_ARCHIVAGE_CHAMPS_SQL')) {
	/**
	 * Liste des champs utilisés par l'archivage et leur déclaration SQL :
	 * - 0 non archivé, 1 archivé
	 * - date de l'archivage ou du désarchivage
	 * - type d'objet du parent racine ayant propagé son archivage ou désarchivage
	 * - id du parent racine ayant propagé son archivage ou désarchivage
	 * - identifiant du motif d'archivage ou de désarchivage
	 */
	define(
		'_ARCHIVAGE_CHAMPS_SQL',
		[
			'est_archive'          => 'tinyint(1) DEFAULT 0 NOT NULL',
			'date_archive'         => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			'objet_racine_archive' => "varchar(25) DEFAULT '' NOT NULL",
			'id_racine_archive'    => 'bigint(21) DEFAULT 0 NOT NULL',
			'motif_archive'        => "varchar(32) DEFAULT '' NOT NULL",
		]
	);
}

/**
 * Vérifie si un type d'objet est autorisé à l'archivage en consultant le paramètrage du plugin.
 *
 * @api
 *
 * @param string $objet Type d'objet
 *
 * @return bool `true` si autorisé, `false` sinon
**/
function archivage_objet_est_autorise(string $objet) : bool {
	static $est_autorise = [];

	if (!isset($est_autorise[$objet])) {
		// On initialise l'autorisation à false cas le plus fréquent
		$est_autorise[$objet] = false;
		if (
			$objet
			and include_spip('base/objets')
			and ($table = table_objet_sql($objet))
		) {
			// Lecture des tables autorisées à l'archivage par l'utilisateur
			include_spip('inc/config');
			$tables_autorisees = lire_config('archivage/tables_autorisees', []);

			// Si l'objet est archivable on autorise l'archivage.
			if (in_array($table, $tables_autorisees)) {
				$est_autorise[$objet] = true;
			}
		}
	}

	return $est_autorise[$objet];
}

/**
 * Renvoie les informations d'archivage d'un objet connu par son id.
 *
 * @api
 *
 * @param string     $objet    Type d'objet comme article
 * @param int        $id_objet Valeur du champ identifiant
 * @param null|array $options  Permet de fournir le nom de la table et du champ id afin d'éviter des appels aux
 *                             fonctions objet.
 *
 * @return array Le tableau des informations d'archivage éventuellement vide.
 */
function archivage_objet_lire(string $objet, int $id_objet, ?array $options = []) : array {
	// Initialisation du tableau des objets archivés.
	// Les tableaux sont toujours indexés par l'objet et l'id objet.
	static $contexte_archivage = [];

	// Si l'objet n'a pas encore été stocké, il faut acquérir les champs d'archivage uniquement.
	if (!isset($contexte_archivage[$objet][$id_objet])) {
		// Détermination de la table SQL et du champ id.
		include_spip('base/objets');
		$table_sql = $options['table'] ?? table_objet_sql($objet);
		$table_id = $options['champ_id'] ?? id_table_objet($objet);

		// Détermination des champs à récupérer et de la condition sur l'objet
		$select = array_keys(_ARCHIVAGE_CHAMPS_SQL);
		$where = [
			$table_id . '=' . (int) $id_objet
		];

		if (!$archivage = sql_fetsel($select, $table_sql, $where)) {
			$archivage = [];
		} else {
			// Complétude du tableau par défaut
			$archivage = array_merge(
				$archivage,
				[
					'etat'       => '',
					'est_enfant' => false,
				]
			);

			// On rajoute un identifiant littéral pour l'état d'archivage :
			// - archivé : est_archive égal à 1
			// - désarchivé : la consignation d'archivage est activée, est_archive égal à 0, la date d'archivage est valide
			// - hors archive (vide) : les autres cas
			if (1 === (int) $archivage['est_archive']) {
				$archivage['etat'] = 'archive';
			} elseif (
				include_spip('inc/config')
				and lire_config('archivage/consigner_desarchivage', '')
				and (0 === (int) $archivage['est_archive'])
				and $archivage['date_archive']
				and ($archivage['date_archive'] !== '0000-00-00 00:00:00')
			) {
				$archivage['etat'] = 'desarchive';
			}

			// On positionne l'indicateur enfant en fonction du contexte
			// - l'état n'est pas hors archive et les champs de la racine sont bien remplis
			if (
				$archivage['etat']
				and $archivage['objet_racine_archive']
				and $archivage['id_racine_archive']
			) {
				$archivage['est_enfant'] = true;
			}
		}

		// Mise en cache des données d'archivage de l'objet.
		$contexte_archivage[$objet][$id_objet] = $archivage;
	}

	return $contexte_archivage[$objet][$id_objet];
}

/**
 * Modifie l'état d'archivage d'un objet.
 * Plusieurs actions sont possibles : archiver, désarchiver, modifier le motif ou effacer toute donnée relative à l'archivage.
 *
 * Il est possible que cette action soit prolongée :
 * - pour propager l'action à des enfants éventuels si l'objet l'accepte,
 * - par un post-traitement sur l'objet
 * Cette fonction ne doit jamais être utilisée pour traiter des enfants lors d'une propagation.
 *
 * @param string      $action   Action à effectuer sur l'état d'archivage de l'objet : archiver, desarchiver, expliquer, effacer.
 * @param string      $objet    Type de l'objet concerné
 * @param int         $id_objet Identifiant de l'objet concerné
 * @param null|string $id_motif Identifiant du motif associé à l'action expliquer ou vide
 * @param null|array  $racine   Id et type de l'objet racine éventuellement rattaché à l'objet concerné par l'action ou tableau vide.
 *                              Cette option n'est utilisable que si la modification de l'archivage est activée sur les descendants.
 *
 * @return bool true si la modification s'est bien passée, false sinon.
 */
function archivage_objet_modifier(string $action, string $objet, int $id_objet, ?string $id_motif = '', ?array $racine = []) : bool {
	// Initialisation de la sortie
	$retour = false;

	// Déterminer les mises à jour sur le contexte d'archivage de l'objet.
	include_spip('inc/config');
	$motif = '';
	$date = '';
	$set = [];
	if ($action === 'archiver') {
		// Demande d'archivage
		// - la date est systématiquement consignée et le motif si la configuration le demande
		$date = date('Y-m-d H:i:s');
		$motif = lire_config('archivage/utiliser_motif', '')
			? archivage_motif_defaut($objet, 'archive')
			: '';
		$set = [
			'est_archive'          => 1,
			'date_archive'         => $date,
			'objet_racine_archive' => '',
			'id_racine_archive'    => 0,
			'motif_archive'        => $motif,
		];
	} elseif ($action === 'desarchiver') {
		// Demande de désarchivage :
		// - si consignation de l'archivage, on conserve l'historique via la date de désarchivage voire le motif
		if (lire_config('archivage/consigner_desarchivage', '')) {
			$date = date('Y-m-d H:i:s');
			$motif = lire_config('archivage/utiliser_motif', '')
				? archivage_motif_defaut($objet, 'desarchive')
				: '';
		}
		$set = [
			'est_archive'          => 0,
			'date_archive'         => $date,
			'objet_racine_archive' => '',
			'id_racine_archive'    => 0,
			'motif_archive'        => $motif,
		];
	} elseif (
		($action === 'expliquer')
		and ($motif = $id_motif)
	) {
		// Demande de modification du motif d'archivage avec celui qui a été choisi par l'utilisateur
		$set = [
			'motif_archive' => $motif,
		];
	} elseif ($action === 'effacer') {
		// Demande de suppression de toutes les données relatives à l'archivage de l'objet
		$set = [
			'est_archive'          => 0,
			'date_archive'         => '',
			'objet_racine_archive' => '',
			'id_racine_archive'    => 0,
			'motif_archive'        => '',
		];
	}

	// Mettre à jour les données d'archive de l'objet concerné voire plus.
	if ($set) {
		// Mise à jour de l'objet désigné
		include_spip('action/editer_objet');
		$erreur = objet_modifier(
			$objet,
			$id_objet,
			$set
		);
		if ($erreur === '') {
			// La modification de l'objet initial a fonctionné
			if ($action !== 'expliquer') {
				// Si l'action n'est pas une modification du motif de la racine, on lance la propagation.
				// -- c'est la fonction de propagation qui vérifie la nécessité de celle-ci
				archivage_objet_modifier_enfants($action, $objet, $id_objet, $date, $racine);
			}

			// Enfin, on appelle un post-traitement d'archivage pour les plugins qui en auraient besoin
			pipeline(
				'post_archivage_objet',
				[
					'args' => [
						'objet'    => $objet,
						'id_objet' => $id_objet,
						'action'   => $action,
						'id_motif' => $motif,
						'date'     => $date,
					],
					'data' => [],
				]
			);

			// Retour ok de la fonction
			$retour = true;
		}
	}

	return $retour;
}

/**
 * Modifie l'état d'archivage des enfants d'un objet racine quelque soit leur profondeur.
 * Seules trois actions sont possibles : archiver, désarchiver ou effacer car le motif d'archivage d'un enfant est
 * toujours fixée en référence à l'action effectuée sur la racine.
 *
 * @internal
 *
 * @param string $action   Action venant d'être effectuée sur l'objet racine : archiver, desarchiver.
 * @param string $objet    Type de l'objet racine à partir duquel propager l'action
 * @param int    $id_objet Identifiant de l'objet racine à partir duquel propager l'action
 * @param string $date     Date de modification de l'état d'archivage de la racine
 * @param array  $racine   Id et type de l'objet racine éventuellement rattaché à l'objet concerné par l'action ou tableau vide.
 *                         Cet argument n'est rempli que si la modification de l'archivage est activée sur les descendants.
 *
 * @return bool true si la modification s'est bien passée, false sinon.
 */
function archivage_objet_modifier_enfants(string $action, string $objet, int $id_objet, string $date, array $racine) : bool {
	// Initialisation de la sortie
	$retour = true;

	// Si le type d'objet possède des enfants, on propage la modification aux enfants existants
	include_spip('base/objets');
	$tables = archivage_table_lister();
	if ($configuration = $tables[table_objet_sql($objet)]) {
		// Chaque enfant de l'objet concerné est mis à jour avec le même état d'archivage correspondant à l'action
		// effectuée sur l'objet propagateur ($objet, $id_objet), la date de cette action et le motif générique indiquant
		// une propagation à partir de l'objet propagateur.
		// -- Initialisation du contexte d'archivage avec l'indicateur d'archivage et la date qui ont toujours
		//    la même valeur que celle de l'objet propagateur
		//    Cette initialisation correspond à l'action effacer.
		$set = [
			'est_archive'          => $action === 'archiver' ? 1 : 0,
			'date_archive'         => $date,
			'objet_racine_archive' => '',
			'id_racine_archive'    => 0,
			'motif_archive'        => ''
		];

		// Mise au point du contexte d'archivage des enfants en fonction des paramètres et de l'action propagée
		include_spip('inc/config');
		if ($action === 'archiver') {
			// On met à jour la racine des enfants (objet propagateur) et le motif éventuellement
			$set['objet_racine_archive'] = $objet;
			$set['id_racine_archive'] = $id_objet;
			if (lire_config('archivage/utiliser_motif', '')) {
				$set['motif_archive'] = 'archive_racine';
			}
		} elseif (
			($action === 'desarchiver')
			and lire_config('archivage/consigner_desarchivage', '')
		) {
			// On met à jour la racine des enfants (objet propagateur) et le motif éventuellement uniquement
			// si la consignation du désarchivage est activée
			$set['objet_racine_archive'] = $objet;
			$set['id_racine_archive'] = $id_objet;
			if (lire_config('archivage/utiliser_motif', '')) {
				$set['motif_archive'] = 'desarchive_racine';
			}
		}

		// Les enfants ne sont modifiables que si ils respectent des conditions d'archivage liées à l'action courante
		// - Demande d'archivage : on inclut tous les objets enfants non archivés (y compris donc ceux qui seraient repérés comme désarchivés)
		// - Demande de désarchivage ou d'effacement :
		//     1. l'objet propageant le désarchivage est la racine d'un archivage
		//        -> on inclut les objets enfants de cet objet propagateur
		//     2. l'objet propageant le désarchivage est un descendant d'une autre racine
		//        -> on inclut les objets enfants de la racine de l'objet propagateur
		//     Le désarchivage et l'effacement diffèrent par le fait que le premier s'applique aux enfants archivés alors que le
		//     deuxième s'applique aux enfants non archivés
		if ($action === 'archiver') {
			$condition_archivage = 'est_archive=0';
		} else {
			if (
				(lire_config('archivage/modifier_enfant', '') === 'on')
				and $racine
			) {
				$condition_archivage = 'objet_racine_archive=' . sql_quote($racine['objet']) . ' AND id_racine_archive=' . $racine['id'];
			} else {
				$condition_archivage = 'objet_racine_archive=' . sql_quote($objet) . ' AND id_racine_archive=' . $id_objet;
			}
			if ($action === 'desarchiver') {
				$condition_archivage .= ' AND est_archive=1';
			}
		}

		// Si l'objet concerné est lui-même hiérarchique on récupère tous les objets de ce type appartenant à la
		// branche dont l'objet est la racine et qui ne sont pas déjà dans l'état cible du parent.
		// Sinon, la branche est réduite à l'objet racine lui-même.
		$ids_branche_racine[] = $id_objet;
		if (array_key_exists($objet, $configuration)) {
			// Trouver la table du type d'objet ainsi que le nom de sa colonne id.
			$table = table_objet_sql($objet);
			$id_table = id_table_objet($objet);

			// On ajoute generation par génération d'enfants en se protégeant des references circulaires
			while (
				$ids_enfant = sql_allfetsel(
					$id_table,
					$table,
					[
						$condition_archivage,
						sql_in($configuration[$objet], $ids_branche_racine),
						sql_in($id_table, $ids_branche_racine, 'NOT')
					]
				)
			) {
				if ($ids_enfant) {
					$ids_branche_racine = array_merge($ids_branche_racine, array_column($ids_enfant, $id_table));
				}
			}
		}

		// On boucle sur chaque type d'objet et on propage la modification
		foreach ($configuration as $_type => $_id_table_racine) {
			// Trouver la table du type d'objet ainsi que le nom de sa colonne id.
			$table = table_objet_sql($_type);
			$id_table = id_table_objet($_type);

			if ($_type === $objet) {
				// On exclut l'objet racine lui-même
				$ids_enfant = array_diff($ids_branche_racine, [$id_objet]);
			} else {
				// On identifie les enfants du type courant dont le parent est un objet de la branche
				// Par configuration, on sait que ces enfants sont autorisés à l'archivage, il est inutile de le tester
				$ids_enfant = sql_allfetsel(
					$id_table,
					$table,
					[
						$condition_archivage,
						sql_in($_id_table_racine, $ids_branche_racine)
					]
				);
				$ids_enfant = array_column($ids_enfant, $id_table);
			}
			// On propage la modification sur les enfants identifiés
			if ($ids_enfant) {
				sql_updateq($table, $set, sql_in($id_table, $ids_enfant));
			}
		}
	}

	return $retour;
}

/**
 * Répertorie les objets archivés ou désarchivés d'un type donné et éventuellement enfants d'un objet parent direct.
 *
 * @api
 *
 * @param string     $objet  Type de l'objet
 * @param string     $etat   Valeur de l'état d'archivage, soit `archive` ou `desarchive`.
 * @param null|array $parent Objet parent direct des objets archivés à répertorier
 *
 * @return array Liste des objets dans l'état demandé avec une structure standard quelque soit le type d'objet
 */
function archivage_objet_repertorier(string $objet, string $etat, ?array $parent = []) : array {
	// Initialisation du tableau de sortie
	$objets = [];

	if (
		archivage_objet_est_autorise($objet)
		and (
			($etat === 'archive')
			or ($etat === 'desarchive')
		)
	) {
		// On calcule le where basés sur l'état et éventuellement le parent
		include_spip('base/objets');
		$id_table_parent = '';
		$where[] = $etat === 'archive' ? 'est_archive=1' : 'est_archive=0 AND date_archive>0';
		if (
			$parent
			and isset($parent['objet'])
			and isset($parent['id_objet'])
			and $table_sql_parent = table_objet_sql($parent['objet'])
			and ($tables_autorisees = archivage_table_lister())
			and (isset($tables_autorisees[$table_sql_parent][$objet]))
		) {
			$where[] = $tables_autorisees[$table_sql_parent][$objet] . '=' . sql_quote($parent['id_objet']);
			$id_table_parent = $tables_autorisees[$table_sql_parent][$objet];
		}

		// Extraction des informations de base sur le type d'objet qui ne sont pas disponibles avec objet_info()
		$table_objet_sql = table_objet_sql($objet);
		$table_objet_id = id_table_objet($objet);

		// On formatte le select pour toujours avoir une structure d'index standard :
		// - le contexte d'archivage
		$select = array_keys(_ARCHIVAGE_CHAMPS_SQL);
		// - l'id de l'objet qui s'appelera toujours id
		$select[] = $table_objet_id . ' AS id';
		// - le titre de l'objet (qu'il s'appelle titre ou nom)
		$trouver_table = charger_fonction('trouver_table', 'base');
		$desc = $trouver_table($table_objet_sql);
		if (isset($desc['titre'])) {
			$select[] = $desc['titre'];
		}
		// - le statut de l'objet si il existe
		if (isset($desc['statut'][0]['champ'])) {
			$select[] = $desc['statut'][0]['champ'];
		}
		// - l'id du parent éventuel qu'on appelera toujours id_parent
		if ($id_table_parent) {
			// On ne retient que le premier parent
			$select[] = $id_table_parent . ' AS id_parent';
		}

		// Recherche des objets
		if ($retour = sql_allfetsel($select, $table_objet_sql, $where)) {
			$objets = $retour;
		}
	}

	return $objets;
}

/**
 * Renvoie la liste des motifs d'archivage ou de désarchivage pour un type d'objet donné.
 *
 * @api
 *
 * @param string $objet Type d'objet comme article
 * @param string $etat  Valeur de l'état d'archivage, soit `archive` ou `desarchive`.
 *
 * @return array Le tableau des motifs d'archivage ou de désarchivage au format [id_motif] = traduction
 */
function archivage_motif_lister(string $objet, string $etat) : array {
	// Construction de la liste des motifs
	$motifs = [];

	// -- Initialisation de la liste par des motifs standard valables pour tous les types d'objets
	//    et pour l'état courant de l'objet. Ces motifs sont fournies par le plugin.
	//    Le motif générique d'archivage forcée par le parent n'est pas insérée dans ce tableau car pas positionnable
	//    par l'utilisateur.
	$ids_motifs = [
		"{$etat}_defaut"
	];

	// -- Ajout des motifs additionnels fournis par d'autres plugins pour le type d'objet et l'état
	//    d'archivage en question.
	$ids_motifs = pipeline(
		'archivage_liste_motifs',
		[
			'args' => [
				'objet' => $objet,
				'etat'  => $etat
			],
			'data' => $ids_motifs,
		]
	);

	// -- Calcul du tableau des motifs pour la saisie
	foreach ($ids_motifs as $_id_motif) {
		$motifs[$_id_motif] = _T("archivage:motif_{$_id_motif}_label");
	}

	return $motifs;
}

/**
 * Renvoie l'identifiant du motif par défaut pour l'archivage ou le désarchivage d'un type d'objet donné.
 *
 * @api
 *
 * @param string $objet Type d'objet comme article
 * @param string $etat  Valeur de l'état d'archivage, soit `archive` ou `desarchive`.
 *
 * @return string L'identifiant du motif par défaut
 */
function archivage_motif_defaut(string $objet, string $etat) : string {
	// Acquisition du motif par défaut récupéré par le pipeline
	$defaut = pipeline(
		'archivage_defaut_motif',
		[
			'args' => [
				'objet' => $objet,
				'etat'  => $etat
			],
			'data' => '',
		]
	);

	// Si le plugin utilisateur ne fournit pas le motif par défaut, Archivage de Contenus fournit le sien
	if (!$defaut) {
		$defaut = "{$etat}_defaut";
	}

	return $defaut;
}

/**
 * Renvoie la traduction du motif d'archivage d'un objet donné.
 * Le contexte d'archivage de l'objet est fournie à la fonction de traduction.
 *
 * @api
 *
 * @param string $objet    Type d'objet comme article
 * @param int    $id_objet Valeur du champ identifiant
 *
 * @return string Traduction du motif associé à l'objet
 */
function archivage_motif_traduire(string $objet, int $id_objet) : string {
	// Initialisation du libellé du motif
	$traduction = '';

	// On vérifie d'abord si le motif est utilisé ou pas
	include_spip('inc/config');
	if (lire_config('archivage/utiliser_motif', '')) {
		// Tableau de l'archivage de l'objet
		include_spip('inc/archivage');
		$archivage = archivage_objet_lire(
			$objet,
			$id_objet
		);

		// Extraction du motif si il est déjà positionné
		if (!empty($archivage['motif_archive'])) {
			$traduction = _T(
				'archivage:motif_' . $archivage['motif_archive'] . '_label',
				array_merge(
					$archivage,
					[
						'titre_racine_archive' => generer_objet_info($archivage['id_racine_archive'], $archivage['objet_racine_archive'], 'titre'),
						'url_racine_archive'   => generer_objet_url($archivage['id_racine_archive'], $archivage['objet_racine_archive'])
					]
				)
			);
		}
	}

	return $traduction;
}

/**
 * Supprime tout ou partie des traces d'archivage dans la base de données.
 * Passe en revue toutes les tables autorisées uniquement ou une liste de tables précisées.
 *
 * @api
 *
 * @param null|string $action      Action de vidage demandé : le vidage est demandé soit globalement (vide) soit sur
 *                                 changement d'un paramètre de configuration.
 * @param null|array  $tables      Liste des tables SQL devant subir l'opération de vidage. Si vide, la réinitialisation
 *                                 s'applique à toutes les tables autorisées à l'archivage
 * @param null|string $type_parent Type d'objet parent de la table à réinitialiser (action = `desactiver_enfants`)
 *
 * @return array Tableau des nombres d'objets modifiés par table SQL.
 */
function archivage_table_reinitialiser(?string $action = '', ?array $tables = [], ?string $type_parent = '') : array {
	// Initialisation de la sortie : vide si erreur
	$retour = [];

	// Détermination des tables concernées
	$tables_concernees = $tables ?: array_keys(archivage_table_lister(true));

	// En fonction de l'action de vidage :
	// - le contexte d'archivage est plus ou moins réinitialisé
	// - et la liste des objets concernés diffère.
	$set = [];
	$where = [];
	if (
		!$action
		or ($action === 'desactiver_tables')
		or ($action === 'desactiver_enfants')
	) {
		// Tout le contexte d'archivage doit être réinitialisé
		$set = [
			'est_archive'          => 0,
			'date_archive'         => '',
			'objet_racine_archive' => '',
			'id_racine_archive'    => 0,
			'motif_archive'        => '',
		];
		// Si l'action est de réinitialiser des tables
		// -- on repère toute trace d'archivage en vérifiant que l'un des champs du contexte possède une valeur
		//    différente de celle par défaut
		// Si l'action est de propager la réinitialisation aux enfants
		// -- on repère tous les enfants dont le contexte d'archivage réfère à une racine du type du parent
		if ($action === 'desactiver_enfants') {
			$where = [
				'objet_racine_archive=' . sql_quote($type_parent),
				'id_racine_archive>0',
			];
		} else {
			$where[] = 'est_archive=1 OR id_racine_archive>0 OR motif_archive!="" OR date_archive>0';
		}
	} elseif ($action === 'consigner_desarchivage') {
		// Seule la date d'archive doit être supprimée, le reste du contexte d'archivage doit être conservé
		$set = [
			'date_archive' => '',
		];
		// On repère les contenus désarchivés
		$where = [
			'est_archive=0',
			'date_archive>0'
		];
	} elseif ($action === 'utiliser_motif') {
		// Seule la date d'archive doit être supprimée, le reste du contexte d'archivage doit être conservé
		$set = [
			'motif_archive' => '',
		];
		$where[] = 'motif_archive!=""';
	}

	// Pour chaque table autorisée à l'archivage on récupère tous les objets ayant une trace d'archivage
	// pour réinitialiser le contexte.
	if ($set) {
		foreach ($tables_concernees as $_table) {
			// On remet à zéro les objets trouvés
			$nb = sql_countsel($_table, $where);
			if ($nb > 0) {
				sql_updateq($_table, $set, $where);
				$retour[$_table] = $nb;
			}
		}
	}

	return $retour;
}

/**
 * Renvoie la liste des tables archivables ainsi que leur configuration si elles possèdent des enfants. On peut filtrer
 * cette liste en ne retenant que les tables autorisées par l'utilisateur (paramétrage du plugin).
 *
 * @api
 *
 * @param null|bool $tables_autorisees_seules Si `true`, on ne retient que les tables choisies pour être archivées. Par défaut,
 *                                            à `false`.
 *
 * @return array Le tableau tables archivables avec leur configuration enfant éventuelle.
 */
function archivage_table_lister(?bool $tables_autorisees_seules = false) : array {
	// Initialisation en static
	static $tables = [];

	if (!isset($tables[$tables_autorisees_seules])) {
		// Initialisation de la liste des types d'objet archivables
		if ($tables_autorisees_seules) {
			// On ne retient que les tables ayant fait l'objet d'un choix d'archivage par l'utilisateur
			include_spip('inc/config');
			$tables_objet = lire_config('archivage/tables_autorisees', []);
		} else {
			// On récupère toutes les tables d'objets déclarées
			include_spip('base/objets');
			$tables_objet = array_keys(lister_tables_objets_sql());

			// On vérifie si il existe des tables exclues de tout archivage
			// -- soit par la constante _ARCHIVAGE_TABLES_EXCLUES
			// -- soit par le pipeline archivage_exclusion_tables
			$tables_exclues = defined('_ARCHIVAGE_TABLES_EXCLUES')
				? constant('_ARCHIVAGE_TABLES_EXCLUES')
				: [];
			$tables_exclues = pipeline(
				'archivage_exclusion_tables',
				[
					'args' => [],
					'data' => $tables_exclues,
				]
			);
			$tables_objet = array_diff($tables_objet, array_unique($tables_exclues));
		}

		// On construit la configuration des enfants des tables archivables
		$tables[$tables_autorisees_seules] = [];
		foreach ($tables_objet as $_table_sql) {
			// On initialise la configuration des enfants
			$tables[$tables_autorisees_seules][$_table_sql] = [];
			// On acquiert les éventuels enfants
			$enfants = objet_type_decrire_infos_enfants(objet_type($_table_sql));
			if ($enfants) {
				foreach ($enfants as $_objet => $_infos) {
					if (in_array(table_objet_sql($_objet), $tables_objet)) {
						$tables[$tables_autorisees_seules][$_table_sql][$_objet] = $_infos['champ'];
					}
				}
			}
		}
	}

	return $tables[$tables_autorisees_seules];
}

/**
 * Déclare les instructions SQL correspondant à la création des champs nécessaires à l'archivage pour une table donnée.
 * Cette fonction est à appeler dans la fonction upgrade().
 *
 * @api
 *
 * @param string $table_sql Table d'objets SQL (par exemple, spip_articles)
 *
 * @return array Liste des instructions de création des champs d'archivage
 */
function archivage_table_upgrade_contexte(string $table_sql) : array {
	$creation = [];
	foreach (_ARCHIVAGE_CHAMPS_SQL as $_champ => $_declaration) {
		$creation[] = ['sql_alter', "TABLE {$table_sql} ADD {$_champ} {$_declaration}"];
	}

	return $creation;
}

/**
 * Ajoute les champs nécessaires à l'archivage pour une table donnée.
 * Cette fonction est appelée dans la configuration si une table a été créée après l'installation du plugin.
 *
 * @api
 *
 * @param string $table_sql Table d'objets SQL (par exemple, spip_articles)
 *
 * @return void
 */
function archivage_table_ajouter_contexte(string $table_sql) : void {
	foreach (_ARCHIVAGE_CHAMPS_SQL as $_champ => $_declaration) {
		sql_alter("TABLE {$table_sql} ADD {$_champ} {$_declaration}");
	}
}

/**
 * Supprime, pour une table SQL archivable, les champs nécessaires à l'archivage.
 * Cette fonction est à appeler dans la fonction vider_tables().
 *
 * @api
 *
 * @param string $table_sql Table d'objets SQL (par exemple, spip_articles)
 *
 * @return void
 */
function archivage_table_supprimer_contexte(string $table_sql) : void {
	// Vérifier que la table existe bien
	$trouver_table = charger_fonction('trouver_table', 'base');
	$description = $trouver_table($table_sql);
	if ($description) {
		foreach (array_keys(_ARCHIVAGE_CHAMPS_SQL) as $_champ) {
			if (isset($description['field'][$_champ])) {
				sql_alter("TABLE {$table_sql} DROP {$_champ}");
			}
		}
	}
}
