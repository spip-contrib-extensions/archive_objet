<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation du schéma de données propre au plugin et gestion des migrations suivant
 * les évolutions du schéma.
 *
 * Le plugin Archivage de contenu ne crée aucune nouvelle table mais ajoute des champs à tous les objets déclarés.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 * @param string $version_cible         Version du schéma de données en fin d'upgrade
 *
 * @return void
 */
function archivage_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	// Initialisation des modifications des tables d'objet
	$maj = [];

	// Paramétrage du plugin par défaut
	$config = [
		'tables_autorisees'      => [],
		'modifier_enfant'        => '',
		'utiliser_motif'         => '',
		'consigner_desarchivage' => '',
	];
	$maj['create'] = [
		['ecrire_config', 'archivage', $config]
	];

	// Récupérer les tables d'objet et rajouter les champs :
	// -- on ne tient compte que des tables d'objet déjà déclarées.
	//    Les tables créées par des plugins à la suite de l'installation du plugin Archivage de Contenus seront
	//    traitées lors de l'autorisation (formulaire de configuration)
	include_spip('inc/archivage');
	include_spip('base/objets');
	$tables_archivables = array_keys(lister_tables_objets_sql());
	foreach ($tables_archivables as $_table) {
		$maj['create'] = array_merge(
			$maj['create'],
			archivage_table_upgrade_contexte($_table)
		);
	}

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Suppression de l'ensemble du schéma de données propre au plugin, c'est-à-dire
 * des champs des tables archivables et des variables de configuration.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 *
 * @return void
 */
function archivage_vider_tables(string $nom_meta_base_version) : void {
	// Récupérer les tables d'objet archivables et supprimer les champs ajoutés :
	include_spip('inc/archivage');
	include_spip('base/objets');
	$tables_archivables = array_keys(lister_tables_objets_sql());
	foreach ($tables_archivables as $_table) {
		archivage_table_supprimer_contexte($_table);
	}

	effacer_meta('archivage');
	effacer_meta($nom_meta_base_version);
}
