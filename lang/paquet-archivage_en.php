<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/archivage-paquet-xml-archive_objet?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// A
	'archivage_description' => 'This plugin provides a reversible way of archiving a SPIP content without affecting its publication status. It records the date of archiving and allows the user to enter the reason for archiving the content.',
	'archivage_nom' => 'Content archiving',
	'archivage_slogan' => 'A way to archive the various contents of a SPIP site',
];
