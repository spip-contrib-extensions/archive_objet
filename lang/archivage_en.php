<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/archivage-archive_objet?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// A
	'action_archiver_label' => 'Archive',
	'action_definir_motif_label' => 'Define an reason',
	'action_desarchiver_label' => 'Unarchive',
	'action_effacer_label' => 'Delete archive data',
	'action_modifier_motif_label' => 'Change reason',
	'action_vider_label' => 'Delete all archiving data from the database',
	'archives_boite_info_message' => 'This page allows site administrators to consult all archives for archivable content types.',
	'archives_onglet_titre' => 'Archived contents',
	'archives_page_titre' => 'List of archived contents',

	// C
	'configuration_archivage_enfant_label' => 'Allows the archiving status of a descendant to be modified independently of the initiating root',
	'configuration_avertissement' => 'Setting up this plugin is extremely sensitive, as it can induce a <strong>partial or total reset of the contents archiving context</strong>.
	Be sure to read the warnings associated with each parameter.',
	'configuration_desarchivage_explication' => 'If you disable unarchiving recording, unarchived contents will be reset.',
	'configuration_desarchivage_label' => 'Recording archiving',
	'configuration_motif_explication' => 'If you disable the use of the reason, content with an archiving reason will have its reason removed.',
	'configuration_motif_label' => 'Specify the reason for each archiving or unarchiving (except for descendants)',
	'configuration_onglet_titre' => 'Archiving settings',
	'configuration_page_titre' => 'Plugin @plugin@',
	'configuration_reinit_consigner_desarchivage' => 'the archive date for unarchived content has been reset : @nb@',
	'configuration_reinit_desactiver_enfants' => 'the following child content types have been reset : @nb@',
	'configuration_reinit_desactiver_tables' => 'the following content types have been reset : @nb@',
	'configuration_reinit_utiliser_motif' => 'the archiving reason has been deleted from the contents in : @nb@',
	'configuration_tables_autorisees_enfants' => 'enfants : @enfants@',
	'configuration_tables_autorisees_erreur' => '@objets@ content type is not allowed, while parent type content is.',
	'configuration_tables_autorisees_explication' => 'If you remove archiving authorization from a content type, contents of that type with archiving data will be reset, as will its children of other types.',
	'configuration_tables_autorisees_label' => 'Select the content types you wish to archive',

	// D
	'date_label' => 'Since',
	'desarchives_onglet_titre' => 'Unarchived contents',
	'desarchives_page_titre' => 'List of unarchived contents',

	// E
	'edition_motif_archive_titre' => 'Change archiving reason',
	'edition_motif_desarchive_titre' => 'Change unarchiving reason',
	'edition_motif_label' => 'Select reason',
	'erreur_modifier_archivage_motif' => 'Error when changing reason.',
	'erreur_modifier_archivage_non_autorisee' => 'You do not have the right to change the reason.',

	// L
	'liste_archive_prefixe_titre' => 'Archives',

	// M
	'menu_titre' => 'Archiving monitoring',
	'motif_archive_defaut_label' => 'standard archiving',
	'motif_archive_racine_label' => 'Content root archiving <a href="@url_racine_archive@">@title_racine_archive@ (@objet_racine_archive@-@id_racine_archive@)</a>',
	'motif_desarchive_defaut_label' => 'archiving error',
	'motif_desarchive_racine_label' => 'unarchiving content root @objet_racine_archive@-@id_racine_archive@',

	// O
	'objet_message_archive' => 'This content was archived on @date@.',
	'objet_message_desarchive' => 'This content was unarchived on @date@.',
	'objet_message_motif' => 'Reason : @motif@.',

	// R
	'racine_label' => 'Content root',
];
