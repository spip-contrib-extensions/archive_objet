<?php
/**
 * Ce fichier contient les critères et balises du plugin Archivage de Contenus.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Définit le critère {archive} et {!archive} plus pratique à utiliser que {est_archive=1} ou
 * {est_archive=0}.
 *
 * @param string  $idb     Identifiant de la boucle
 * @param array   $boucles AST du squelette
 * @param Critere $critere Paramètres du critère dans cette boucle
 *
 * @return void
 */
function critere_archive_dist(string $idb, array &$boucles, Critere $critere) : void {
	// Initialisation de la table sur laquelle porte le critère
	include_spip('base/objets');
	$boucle = &$boucles[$idb];
	$table_objet = $boucle->id_table;
	$table = table_objet_sql($table_objet);

	// Vérifier que la table fait bien partie de la liste autorisée à utiliser l'archivage.
	include_spip('inc/config');
	$tables_autorisees = lire_config('archivage/tables_autorisees', []);
	if (in_array($table, $tables_autorisees)) {
		// Définition de la valeur du champ est_archive en fonction de l'existence du not ou pas
		$valeur = ($critere->not == '!') ? 0 : 1;

		// Création du critère sur le champ 'est_archive'.
		$boucle->where[] = ["'='", "'{$table_objet}.est_archive'", $valeur];
	}
}

/**
 * Définit le critère {desarchive} qui permet, si la consignation du désarchivage est activée, de sélectionner
 * plus simplement les objets désarchivés.
 *
 * Si la consignation du désarchivage est désactivée, le critère ne retourne aucun objet. Sinon, il correspond
 * à la conjonction des critères {est_archive=0} et {date_archive>0}.
 *
 * @param string  $idb     Identifiant de la boucle
 * @param array   $boucles AST du squelette
 * @param Critere $critere Paramètres du critère dans cette boucle
 *
 * @return void
 */
function critere_desarchive_dist(string $idb, array &$boucles, Critere $critere) : void {
	// Initialisation de la table sur laquelle porte le critère
	include_spip('base/objets');
	$boucle = &$boucles[$idb];
	$table_objet = $boucle->id_table;
	$table = table_objet_sql($table_objet);

	// Vérifier que la table fait bien partie de la liste autorisée à utiliser l'archivage.
	include_spip('inc/config');
	$tables_autorisees = lire_config('archivage/tables_autorisees', []);
	if (in_array($table, $tables_autorisees)) {
		// Création du critère sur le champ 'est_archive'.
		$boucle->where[] = ["'='", "'{$table_objet}.est_archive'", 0];

		// Création du critère sur le champ 'date_archive'.
		//  -- si la consignation du désarchivage est activée cette condition sera fausse
		$boucle->where[] = ["'>'", "'{$table_objet}.date_archive'", '0'];
	}
}

/**
 * Définit le critere {ignorer_archivage} qui permet de rendre inopérant la restriction de l'archivage sur une boucle.
 *
 * @param string  $idb     Identifiant de la boucle
 * @param array   $boucles AST du squelette
 * @param Critere $critere Paramètres du critère dans cette boucle
 *
 * @return void
 */
function critere_ignorer_archivage_dist(string $idb, array &$boucles, Critere $critere) : void {
	$boucle = &$boucles[$idb];
	$boucle->modificateur['ignorer_archivage'] = true;
}

/**
 * Compile la balise `#ARCHIVAGE_TABLES` qui renvoie la liste des tables d'objets archivables ou autorisées suivant
 * l'argument passé.
 * La signature de la balise est : `#ARCHIVAGE_TABLES{[tables_autorisees_seules]}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ARCHIVAGE_TABLES_dist(Champ $p) : Champ {
	$tables_autorisees_seules = interprete_argument_balise(1, $p);
	$tables_autorisees_seules = isset($tables_autorisees_seules) ?? '0';

	// Aucun argument à la balise.
	$p->code = "(include_spip('inc/archivage')
			? archivage_table_lister((bool) {$tables_autorisees_seules})
			: [])";

	return $p;
}

/**
 * Compile la balise `#ARCHIVAGE_CONTEXTE` qui renvoie tout le contexte d'archivage étendu d'un objet.
 * La signature de la balise est : `#ARCHIVAGE_CONTEXTE{objet, id_objet}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ARCHIVAGE_CONTEXTE_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	$objet = interprete_argument_balise(1, $p);
	$objet = str_replace('\'', '"', $objet);
	$id_objet = interprete_argument_balise(2, $p);
	$id_objet = $id_objet ?? '0';

	// Calcul de la balise
	$p->code = "(include_spip('inc/archivage')
			? archivage_objet_lire({$objet}, {$id_objet})
			: [])";

	return $p;
}

/**
 * Compile la balise `#ARCHIVAGE_LISTE` qui répertorie les objets d'un type donné dont l'état d'archivage vaut archivé
 * ou désarchivé et qui éventuellement sont des enfants d'un objet parent donné.
 * La signature de la balise est : `#ARCHIVAGE_LISTE{objet, etat[, parent]}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ARCHIVAGE_LISTE_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	$objet = interprete_argument_balise(1, $p);
	$objet = str_replace('\'', '"', $objet);
	$etat = interprete_argument_balise(2, $p);
	$etat = str_replace('\'', '"', $etat);
	$parent = interprete_argument_balise(3, $p);
	$parent = isset($parent) ? str_replace('\'', '"', $parent) : '[]';

	// Calcul de la balise
	$p->code = "(include_spip('inc/archivage')
			? archivage_objet_repertorier({$objet}, {$etat}, {$parent})
			: [])";

	return $p;
}

/**
 * Compile la balise `#ARCHIVAGE_MOTIF` qui renvoie le texte traduit du motif d'archivage.
 * La signature de la balise est : `#ARCHIVAGE_MOTIF{objet, id_objet}`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ARCHIVAGE_MOTIF_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	$objet = interprete_argument_balise(1, $p);
	$objet = str_replace('\'', '"', $objet);
	$id_objet = interprete_argument_balise(2, $p);
	$id_objet = $id_objet ?? '0';

	// Calcul de la balise
	$p->code = "(include_spip('inc/archivage')
			? archivage_motif_traduire({$objet}, {$id_objet})
			: '')";

	return $p;
}
