<?php
/**
 * Ce fichier contient les cas d'utilisation des pipelines d'affichage, d'édition et d'autorisation.
 *
 * @package SPIP\ARCHIVAGE\PIPELINES
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Affichage, dans la fiche d'un objet autorisé à être archivé, d'un bloc identifiant
 * l'état d'archivage, la date d'archivage et le motif.
 *
 * @pipeline affiche_milieu
 *
 * @param array $flux Flux d'entrée contenant la chaine affichée
 *
 * @return array Flux complétée
 */
function archivage_affiche_milieu(array $flux) : array {
	if (isset($flux['args']['exec'])) {
		// Initialisation de la page du privé
		$exec = $flux['args']['exec'];

		if (
			($objet_exec = trouver_objet_exec($exec))
			and !$objet_exec['edition']
			and ($objet = $objet_exec['type'])
			and include_spip('inc/archivage')
			and archivage_objet_est_autorise($objet)
			and ($id_objet = (int) ($flux['args'][$objet_exec['id_table_objet']]))
		) {
			// Fiche d'un objet archivable : message d'archivage si besoin
			// -- Contexte d'archivage.
			$contexte_archivage = archivage_objet_lire(
				$objet,
				$id_objet,
				[
					'table'    => $objet_exec['table_objet_sql'],
					'champ_id' => $objet_exec['id_table_objet']
				]
			);

			// -- Vérifier les conditions d'affichage du message d'archivage.
			if ($contexte_archivage['etat']) {
				// Construction du contexte pour l'inclusion affichant le message d'archivage.
				$contexte = array_merge(
					$contexte_archivage,
					[
						'objet'    => $objet,
						'id_objet' => $id_objet,
					]
				);

				if ($texte = recuperer_fond('prive/squelettes/inclure/inc-archivage_fiche_objet', $contexte, ['trim'])) {
					if ($pos = strpos($flux['data'], '<!--affiche_milieu-->')) {
						$flux['data'] = substr_replace($flux['data'], $texte, $pos, 0);
					} else {
						$flux['data'] .= $texte;
					}
				}
			}
		}
	}

	return $flux;
}

/**
 * Insertion dans le pipeline boite_infos (SPIP)
 * Rajouter, pour les objets archivables un bouton d'archivage ou de désarchivage suivant l'état courant.
 *
 * @pipeline boite_infos
 *
 * @param array $flux Flux d'entrée contenant la chaine affichée
 *
 * @return array Flux complétée
 */
function archivage_boite_infos(array $flux) : array {
	if (isset($flux['args']['type'])) {
		// Initialisation du type d'objet concerné.
		$objet = $flux['args']['type'];

		if (
			($objet_exec = trouver_objet_exec($objet))
			and !$objet_exec['edition']
			and ($objet = $objet_exec['type'])
			and include_spip('inc/archivage')
			and archivage_objet_est_autorise($objet)
			and ($id_objet = (int) ($flux['args']['id']))
		) {
			// Page d'un objet archivable : afficher le bouton adéquat.
			// -- Acquérir le contexte d'archivage.
			$contexte_archivage = archivage_objet_lire(
				$objet,
				$id_objet,
				[
					'table'    => $objet_exec['table_objet_sql'],
					'champ_id' => $objet_exec['id_table_objet']
				]
			);

			// -- Inclure le bouton archiver ou désarchiver
			$contexte = array_merge(
				$contexte_archivage,
				[
					'action'   => !$contexte_archivage['est_archive'] ? 'archiver' : 'desarchiver',
					'objet'    => $objet,
					'id_objet' => $id_objet,
				]
			);
			if (
				autoriser('modifierarchivage', $objet, $id_objet, null, $contexte)
				and ($bouton = recuperer_fond('prive/squelettes/inclure/inc-archivage_boite_infos_objet', $contexte, ['trim']))
			) {
				$flux['data'] .= $bouton;
			}
		}
	}

	return $flux;
}
/**
 * Filtrer les boucles pour ne pas afficher les objets archivés par défaut sauf si un critère
 * d'archivage explicite ou conditionnel existe.
 *
 * @pipeline post_boucle
 *
 * @param Boucle $boucle Objet boucle de SPIP correspond à la boucle en cours de traitement.
 *
 * @return Boucle La boucle dont la condition `where` a été modifiée ou pas.
 */
function archivage_post_boucle(Boucle $boucle) : Boucle {
	// Initialisation de la table sur laquelle porte le critère
	include_spip('base/objets');
	if ($table_objet = $boucle->id_table) {
		$table = table_objet_sql($table_objet);
		$id_table = id_table_objet($table_objet);

		// On ne filtre pas la boucle hiérarchie sinon on perd le fil d'ariane
		if (
			$table
			and $boucle->iterateur === 'SQL'
			and $boucle->type_requete !== 'hierarchie'
			and empty($boucle->modificateur['ignorer_archivage'])
		) {
			// Vérifier que la table fait bien partie de la liste autorisée à utiliser l'archivage.
			include_spip('inc/config');
			$tables_autorisees = lire_config('archivage/tables_autorisees', []);
			if (in_array($table, $tables_autorisees)) {
				// On cherche un critère d'archivage explicite parmi :
				// - {est_archive = 0} ou {est_archive = 1}
				// - {archive} ou {!archive}
				// Ou un critère explicite sur l'id de la table comme {id_article} ou {id_article=xxx}
				// --> Si il existe un tel critère, alors on n'exclut pas les archives par défaut.
				//
				// Les critères {id_article?} ou {id_article IN x,y,z} ne sont pas considérés comme une recherche unique
				// et donc le critère d'archivage doit s'appliquer par défaut.
				$critere_archive_explicite = false;
				$criteres = $boucle->criteres;
				foreach ($criteres as $_critere) {
					if (
						($_critere->op == 'archive')
						or (
							($_critere->op == $id_table)
							and (!$_critere->cond)
						)
						or (
							!empty($_critere->param[0][0]->texte)
							and (
								($_critere->param[0][0]->texte == 'est_archive')
								or (
									($_critere->param[0][0]->texte == $id_table)
									and ($_critere->op == '=')
								)
							)
						)
					) {
						$critere_archive_explicite = true;
						break;
					}
				}

				// Si on a trouvé aucun critère explicite, il se peut qu'il y ait un critère conditionnel du type {est_archive?} :
				// dans ce cas, la condition a déjà été calculée mais pas de la bonne façon.
				// -> Il faut donc la corriger et ne pas rajouter la condition d'exclusion des archives.
				if (!$critere_archive_explicite) {
					$conditions = $boucle->where;
					foreach ($conditions as $_cle => $_condition) {
						if (
							(strpos($_condition[0], '?') !== false)
							and (strpos($_condition[1], 'est_archive') !== false)
						) {
							// On met à jour cette condition pour que l'absence de est_archive dans l'env
							// implique l'ajout de est_archive=0.
							// TODO : tant que le core n'accepte pas la fonction generer_where_est_archive il faut bidouiller.
							$where_est_archive[0] = $_condition[0];
							$where_est_archive[1] = $_condition[1];
							$where_est_archive[2] = ["'='", "'{$table_objet}.est_archive'", 0];
							$where_est_archive[3] = $_condition[3][3];
							$boucle->where[$_cle] = $where_est_archive;
							$critere_archive_explicite = true;
							break;
						}
					}

					// Aucun critère d'archivage explicite ni conditionnel : on peut filtrer la boucle en excluant les archives.
					if (!$critere_archive_explicite) {
						$boucle->where[] = ["'='", "'{$table_objet}.est_archive'", 0];
					}
				}
			}
		}
	}

	return $boucle;
}

/**
 * Surcharge de la fonction charger des formulaires concernes, a savoir :
 * - objet / instituer_objet : dans la page de l'objet en cours bloque le statut de l'objet si celui-ci est archivé.
 *
 * @param array $flux Flux d'entrée pour le chargement du formulaire
 *
 * @return array Flux complétée
 *
 **/
function archivage_formulaire_charger(array $flux) : array {
	// Identifier le formulaire
	$form = $flux['args']['form'];

	// Filtrer le formulaire instituer_objet
	if (
		($form === 'instituer_objet')
		&& $flux['data']
	) {
		$objet = !empty($flux['data']['objet']) ? $flux['data']['objet'] : $flux['data']['_objet'];
		$id_objet = !empty($flux['data']['id_objet']) ? (int) ($flux['data']['id_objet']) : (int) ($flux['data']['_id_objet']);

		// Vérifier que la table fait bien partie de la liste autorisée à utiliser l'archivage.
		include_spip('inc/archivage');
		if (archivage_objet_est_autorise($objet)) {
			// Acquérir le contexte d'archivage.
			$contexte_archivage = archivage_objet_lire(
				$objet,
				$id_objet,
			);

			// Si l'objet est archivé on bloque l'édition du formulaire instituer.
			if ($contexte_archivage['est_archive']) {
				$flux['data']['editable'] = false;
			}
		}
	}

	return $flux;
}

/**
 * Affichage, dans la zone prévues à cet effet, des éventuels enfants d'un objet archivé.
 *
 * @pipeline affiche_milieu
 *
 * @param array $flux Flux d'entrée contenant la chaine affichée
 *
 * @return array Flux complétée
 */
function archivage_affiche_enfants(array $flux) : array {
	if (isset($flux['args']['exec'])) {
		// Initialisation de la page du privé
		$exec = $flux['args']['exec'];

		if (
			($objet_exec = trouver_objet_exec($exec))
			and !$objet_exec['edition']
			and ($table_parent = $objet_exec['table_objet_sql'])
			and include_spip('inc/archivage')
			and $config_objets = archivage_table_lister()
			and !empty($config_objets[$table_parent])
			and ($objet_parent = $objet_exec['type'])
			and ($id_objet_parent = (int) ($flux['args']['id_objet']))
	) {
			// Page d'un objet archivable qui possède des objets enfants, on ajoute la liste des enfants,
			// pour chaque type d'enfants, sauf pour les rubriques dont la liste des sous-rubriques est spécifique
			foreach ($config_objets[$table_parent] as $_objet_enfant => $_id_table_parent) {
				if (archivage_objet_est_autorise($_objet_enfant)) {
					// Le contexte d'archivage doit fonctionner pour le squelette `archives` et les personnalisation `<objets>_archives`
					// - parent_objet et parent_id sont nommés ainsi pour éviter une collision avec $_id_table_parent qui
					//   sert aux fonds personnalisés
					$table_objet_enfant = table_objet($_objet_enfant);
					$contexte = [
						// Pour les squelettes personnalisés
						'est_archive'    => 1,
						// Pour le squelette standard archives.html
						'etat_archivage' => 'archive',
						'type_objet'     => $_objet_enfant,
						'parent_objet'   => $objet_parent,
						'parent_id'      => $id_objet_parent,
						// Pour les deux
						'sinon'          => '',
						'par'            => 'date_archive',
						'prefixe_titre'  => _T('archivage:liste_archive_prefixe_titre')
					];
					if (isset($flux['args']['id_objet'])) {
						// Pour les squelettes personnalisés
						$contexte[$_id_table_parent] = (int) ($flux['args']['id_objet']);
					}

					// On cherche le fond à utiliser : la personnalisation <objets>_archives.html en premier ou archives.html sinon
					include_spip('inc/utils');
					$fond = "prive/objets/liste/{$table_objet_enfant}_archives";
					if (!trouver_fond($fond)) {
						$fond = 'prive/objets/liste/archives';
					}

					if ($texte = recuperer_fond($fond, $contexte, ['trim'])) {
						$flux['data'] .= $texte;
					}
				}
			}
		}
	}

	return $flux;
}
